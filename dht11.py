# blink.py
import Adafruit_DHT
import time

senser = Adafruit_DHT.DHT11
dht_pin = 2

while True:
	humidity, temperature = Adafruit_DHT.read_retry(senser, dht_pin)
	if humidity is not None and temperature is not None:
		print("Temp = {0}*C Humidity = {1}%". format(temperature, humidity))
	else :
		print("Fail")
	time.sleep(2)
