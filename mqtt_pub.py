# blink.py
import paho.mqtt.client as mqtt
import time
import Adafruit_DHT

MQTT_HOST = "test.mosquitto.org"
MQTT_PORT = 1883
MQTT_KEEPALIVE_INTERVAL = 60
MQTT_TOPIC = "harang"

senser = Adafruit_DHT.DHT11
dht_pin = 2

def on_publish(client, userdata, mid):
	print("MEssage Published...")
	
client = mqtt.Client()
client.on_publish = on_publish
client.connect(MQTT_HOST, MQTT_PORT, MQTT_KEEPALIVE_INTERVAL)

while True:
	try:
		time.sleep(1)
		humidity, temperature = Adafruit_DHT.read_retry(senser, dht_pin)
		value = '{{"temp":{}, "humid":{} }}'.format(temperature, humidity)
		client.publish(MQTT_TOPIC, value)
		print(value)
	except KeyboardInterrupt:
		break

client.loop_forever()
client.disconnect()
